import React, { useEffect, useState } from "react";

// git test
// git revert test 2
// git revert test 3
// git revert test 4

function ConferenceForm (props) {
    const [locations, setLocations] = useState([])

    const fetchData = async () => {
        const url = "http://localhost:8000/api/locations/"

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();

            setLocations(data.locations)
        }
    }
    // conference name handler
    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    // start date handler
    const [startDate, setStartDate] = useState('');
    const handleStartDateChange = (event) => {
        const value = event.target.value;
        setStartDate(value);
    }
    // end date handler
    const [endDate, setEndDate] = useState('');
    const handleEndDateChange = (event) => {
        const value = event.target.value;
        setEndDate(value);
        }
    // description handler
    const [description, setDescription] = useState('');
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }
    // max presentations handler
    const [maxPresentations, setMaxPresentations] = useState('');
    const handleMaxPresentationsChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    }
    // max attendees handler
    const [maxAttendees, setMaxAttendees] = useState('');
    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    }
    // location handler
    const [location, setLocation] = useState('');
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    // form submit handler
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.starts = startDate;
        data.ends = endDate;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;

        console.log(data);

        const conferenceUrl = "http://localhost:8000/api/conferences/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            // reset form inputs
            setName('');
            setStartDate('');
            setEndDate('');
            setDescription('');
            setMaxPresentations('');
            setMaxAttendees('');
            setLocation('');
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new conference</h1>
                        <form onSubmit={handleSubmit} id="create-conference-form">
                            <div className="form-floating mb-3">
                                <input value={name} onChange={handleNameChange} placeholder="name" required type="text" name="name" id="name" className="form-control"/>
                                <label htmlFor="name">Conference name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={startDate} onChange={handleStartDateChange} placeholder="starts" required type="date" name="starts" id="starts" className="form-control"/>
                                <label htmlFor="starts">Start date</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={endDate} onChange={handleEndDateChange} placeholder="ends" required type="date" name="ends" id="ends" className="form-control"/>
                                <label htmlFor="ends">End date</label>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="description" className="form-label">description</label>
                                <textarea value={description} onChange={handleDescriptionChange} name="description" id="description" rows="10" className="form-control"></textarea>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={maxPresentations} onChange={handleMaxPresentationsChange} placeholder="max_presentations" type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                                <label htmlFor="max_presentations">Maximum number of presentations</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={maxAttendees} onChange={handleMaxAttendeesChange} placeholder="max_attendees" type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                                <label htmlFor="max_attendees">Maximum number of attendees</label>
                            </div>
                            <div className="mb-3">
                                <select value={location} onChange={handleLocationChange} name="location" id="location" className="form-select">
                                    <option value="">Choose location</option>
                                    {locations.map(location => {
                                        return (
                                            <option key={location.id} value={location.id}>
                                                {location.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
    );
}



export default ConferenceForm;
