function createCard(name, location, description, pictureUrl, starts, ends) {
    return `
            <div class="card" style="margin-bottom: 30px; box-shadow: 8px 8px 10px lightblue;">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                    <p class="card-text">${description}</p>
                </div>
                <div class="card-footer">
                    ${starts} - ${ends}
                </div>
            </div>
    `;
}

function alertMessage(e){
    return `<div class="alert alert-danger" role="alert">${e}</div>`
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok){
            const html = alertMessage("Error: bad request");
            const column = document.querySelector('.column-1');
            column.innerHTML += html;
            console.log('bad request')
        } else {
            const data = await response.json();

            for (let [index, conference] of data.conferences.entries()){
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    console.log("details: ", details)

                    const name = details.conference.name;
                    const location = details.conference.location.name
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;

                    const starts = (new Date(details.conference.starts)).toLocaleDateString();
                    const ends = (new Date(details.conference.ends)).toLocaleDateString();
                    console.log("starts: ", starts, "ends:", ends);

                    const html = createCard(name, location, description, pictureUrl, starts, ends);
                    const column = document.querySelector(`.column-${index % 3 + 1}`)
                    column.innerHTML += html;
                    console.log(html);
                }

            }


        }
    } catch (e) {
        const html = alertMessage(e);
        const column = document.querySelector('.column-1');
        column.innerHTML += html;
        console.log(`error: ${e}`)
    }
});
// const conferenceInformation = details.conference;
// const conferenceDescription = document.querySelector('.card-text');
// conferenceDescription.innerHTML = conferenceInformation.description;

// console.log("information: ", conferenceInformation);
// const imageTag = document.querySelector('.card-img-top');
// imageTag.src = details.conference.location.picture_url;

// conference = data.conferences[0];
// const nameTag = document.querySelector('.card-title');
// nameTag.innerHTML = conference.name;
