console.log("new-presentation.js is running broh")

// create event listener - api call to conferences
window.addEventListener('DOMContentLoaded', async () => {
    // set pull down menu with conference data
    const url = "http://localhost:8000/api/conferences/"
    // fetch conference data
    const response = await fetch(url);
    if (response.ok) {
        // console.log("request is ok broh!")
        const data = await response.json();
        console.log("data => ", data);

        // get the select tag element by its id 'conference'
        const selectTag = document.getElementById('conference')

        for (let conference of data.conferences){
            // console.log("conference => ", conference)
            // create an option element
            let option = document.createElement("option");
            // set the calue property of the option element to the location id
            option.value = conference.id
            // console.log(option)
            option.innerHTML = conference.name
            selectTag.append(option);
            console.log("conferenceId =>", conference.id)
        }
    }
    // get form data
    const formTag = document.getElementById('create-presentation-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const selectTag = document.getElementById("conference");
        const conferenceId = selectTag.options[selectTag.selectedIndex].value
        // console.log("selectTag:", selectTag)


        // console.log("log conference test:", conference.id)
        const presentationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`
        // console.log("presentationURL: ", presentationUrl)
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok){
            formTag.reset();
            const newPresentation = await response.json();
            console.log(newPresentation);
        }

    })

})
