console.log("new-conference.js is running broh")

//create event listener - api call to location list
window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/locations/"
    // fetch location data
    const response = await fetch(url);
    if (response.ok) {
        // console.log("request ok!")
        const data = await response.json();
        // console.log("data ==> ", data)

        // get the select tag element by its id 'location
        const selectTag = document.getElementById('location')

        for (let location of data.locations){  // for each location in the location list
            // console.log("location ==> ", location)
            // create an option element
            let option = document.createElement("option")
            // set the value property of the option element to the location id
            option.value = location.id
            // set the innerHTML to the location name
            option.innerHTML = location.name
            // append the option element as a child of the select tag
            selectTag.append(option)
            // console.log("option: ", option)
        }
    }
    // Get form data
    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const conferenceUrl = "http://localhost:8000/api/conferences/"
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            console.log(newConference);
        }
    })

})
